import React, {useState} from 'react';
import { Platform, Button, Keyboard, StyleSheet, Text, View, TextInput, TouchableOpacity, KeyboardAvoidingView  } from 'react-native';

export default function App () {
  const [task, setTask] = useState();
  const [taskcontent, setTaskItems] = useState([]);
  
  const Task = (data) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.bullet}>
                </View>
                <Text style={styles.itemText}>{data.text}</Text>
            </View>
        </View>
    )
    }  

  const addTask = () => {
    Keyboard.dismiss();
    setTaskItems([task,...taskcontent])
    setTask(null);  
    }

    const completeTask = (index) => {
      let itemsCopy = [...taskcontent];
      itemsCopy.splice(index, 1);
      setTaskItems(itemsCopy);
    }
  
  return (
    <View style = {styles.container}>
      <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.txtBox}
      >
        <TextInput style={styles.input} placeholder= {'Write a task'} value={task} onChangeText={text => setTask(text)} />
      </KeyboardAvoidingView>

      <View style = {styles.butcont}>
        <Button title = "Add task" onPress={() =>addTask()} color = '#ababab'/>
      </View>

      <View style={styles.tasksWrapper}>
        <View style={styles.items}> 
          {
            taskcontent.map((item, index) => {
              return (
                <TouchableOpacity>
                  <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                    <View style={styles.checkbutton}>
                    <Text style={styles.check}>✓</Text> 
                  </View>
                  </TouchableOpacity>
                  <Task text={item} />
                </TouchableOpacity>
              )
            })
          }
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  tasksWrapper:{
    paddingTop: 135,
    paddingHorizontal:23,
  },
  items:{
    marginTop: 25,
  },
  txtBox: {
    position: 'absolute',
    top: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: '#FFF',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    width: 350,
  },
  butcont: {
    top: 130,
    alignItems: "center",
    justifyContent: "center",
  },
  item: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
    width: 325,
    padding: 23,
    borderRadius:10,
    left: 50,
    bottom: 43,
  },
  itemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    },
  checkbutton: {
    width: 28,
    height: 28,
    backgroundColor: '#ababab',
    borderColor: 'black',
    borderWidth: 1,
    opacity: 0.4,
    borderRadius: 20,
    marginRight: 15,
    },
  itemText: {
    maxWidth: '80%',
    },
  check: {
    fontSize: 20,
    bottom: 1,
    marginLeft: 5,
    fontWeight: 'bold',
    }
});
